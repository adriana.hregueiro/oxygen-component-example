# Instructions

You can install this folder as a WP plugin and test it.
For testing this Oxygen component you must open the builder in some page and search for the component named `Test element` under a tab called `My Custom elements`.

![image-20220218121515158](C:\Users\Usuario\AppData\Roaming\Typora\typora-user-images\image-20220218121515158.png)

Al añadir este elemento lo que hace es añadir un botón azul. Tiene varias opciones de configuración puestas:

![image-20220218121603150](C:\Users\Usuario\AppData\Roaming\Typora\typora-user-images\image-20220218121603150.png)

![image-20220218121613774](C:\Users\Usuario\AppData\Roaming\Typora\typora-user-images\image-20220218121613774.png)
