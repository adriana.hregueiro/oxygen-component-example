<?php
/**
 * Plugin Name: Oxygen Extras
 * Version: 1.0.0
 * Description: Add extra visual elements to Oxygen builder
 * Text Domain: oxygen_extras
 */

defined( 'ABSPATH' ) || exit;

/* load_plugin_last */
add_action( 'wp_enqueue_scripts', 'oxygen_extras_wp_enqueue_scripts' );
add_action( 'plugins_loaded', 'oxygen_extras_load_plugin' );

function oxygen_extras_wp_enqueue_scripts() {
  wp_enqueue_style( 'oxygen_extras', plugin_dir_url( __FILE__ ) . 'css/oextras-styles.css', array(), '1.0.0', 'all' );
  wp_enqueue_script( 'oxygen_extras', plugin_dir_url( __FILE__ ) . 'js/oextras-scripts.js', array(), '1.0.0', true );
}

function oxygen_extras_load_plugin() {
	/* check if Oxygen is active */
	if ( ! class_exists( 'OxyEl' ) ) {
		return;
	}
	/* include our plugin main class */
	include_once 'inc/CM_OXY_INTEGRATION.php';

	global $cm_oxy_integration;
	$cm_oxy_integration = new CM_OXY_INTEGRATION();

	/* Our Base Oxygen Element to extend upon */
	include_once 'inc/CUSTOM_OXY_BASE_ELEMENT.php';

	/* Include all of our Oxygen Elements dynamically */
	$elements_filenames = glob( plugin_dir_path( __FILE__ ) . 'elements/*.php' );
	foreach ( $elements_filenames as $filename ) {
		include_once $filename;
	}
}
